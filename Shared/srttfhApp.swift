//
//  srttfhApp.swift
//  Shared
//
//  Created by   Pain on 09.03.2022.
//

import SwiftUI

@main
struct srttfhApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
